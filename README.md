### Hi  👋      Olá  👋 
<br>
- Este projeto tem o intuito de facilitar a imersäo no framework cypress, tranzendo exemplos de cenários básicos á avançados em testes de user inferface e api.
</br>
- This one project has intention at facilitate the immersion in framework cypress, broung examples the scenarios basic to advanced in the tests at user interface and API.

<br>
<br>

![YOUR github stats](https://github-readme-stats.vercel.app/api?username=guilhermealegria&show_icons=true)

[![CI Cypress Template](https://github.com/guilhermealegria/CypressTemplate/actions/workflows/ci.yml/badge.svg)](https://github.com/guilhermealegria/CypressTemplate/actions/workflows/ci.yml)

[![Badge](https://img.shields.io/badge/cypress-9.6.0-green/)](https://www.cypress.io/) [![Badge](https://img.shields.io/badge/node-16.16.0-blueviolet)](https://nodejs.org/en/) [![Badge](https://img.shields.io/badge/npm-8.11.0-red)](https://www.npmjs.com/)
